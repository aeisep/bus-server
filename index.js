var express = require('express');
const consola = require('consola')
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

consola.start('Starting Bus Server');

server.listen(port, () => {
  consola.success(`Server listening at port ${port}`);
});

app.get('/', (req, res) => res.send('Socket Online.'));

var numBus = 0;

io.on('connection', (socket) => {
  var addedBus = false;

  socket.on('updateLocation', (data) => {
    if (!addedBus) return;

    consola.info("Bus location updated: " + JSON.stringify(data));

    socket.broadcast.emit('busLocation', {
      bus: socket.bus,
      location: data
    });
  });

  socket.on('addBus', (data) => {
    if (data.secret === 'OLD_SECRET') {
      socket.bus = data.bus;
      consola.success('Bus id changed.');

      if (addedBus) return;

      ++numBus;
      addedBus = true;

      consola.success('New bus added.');
    } else {
      consola.error('Wrong secret sent by bus.');
    }
  });

  socket.on('disconnect', () => {
    if (addedBus) {
      --numBus;

      socket.broadcast.emit('busLeft', {
        bus: socket.bus,
        numBus: numBus
      });
    }
  });
});